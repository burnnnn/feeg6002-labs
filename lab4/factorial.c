#include <limits.h>
#include <float.h>
#include <math.h>
#include <stdio.h>

long maxlong(void) {
    return LONG_MAX;
}

double upper_bound(long n){
    if(n<6)
    {
        return 719;
    } 
    else 
    {
        return pow(n/2.,n);
    }
}

long factorial(long n){
    long i;
    long fact = 1;

    if (n<0){
        return -2;
    } else if(upper_bound(n) > maxlong()) {
        return -1;
    } else {      
        for (i = 1; i < n+1; i++)
        {
            fact *= i;
        }
        return fact;   
    }
}

int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    for (i=0; i<22; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }


    for (i=1; i<22; i++) {
        printf("factorial(%li)=%li\n", i, factorial(i));
    }



    return 0;
}
